"    powerline_shell_Alt.vim
"    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
"                                            <neva_blyad@lovecri.es>
"
"    This file is part of powerline_shell.
"
"    powerline_shell is free software: you can redistribute it and/or modify
"    it under the terms of the GNU General Public License as published by
"    the Free Software Foundation, either version 3 of the License, or
"    (at your option) any later version.
"
"    powerline_shell is distributed in the hope that it will be useful,
"    but WITHOUT ANY WARRANTY; without even the implied warranty of
"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"    GNU General Public License for more details.
"
"    You should have received a copy of the GNU General Public License
"    along with powerline_shell.  If not, see <https://www.gnu.org/licenses/>.

scriptencoding utf-8

let s:DeepSkyBlue3 = ["#FFFFFF", "#0087AF", 231,  31, ""]
let s:Grey15       = ["#BCBCBC", "#262626", 250, 235, ""]
let s:Grey19       = ["#BCBCBC", "#303030", 250, 236, ""]
let s:Grey23       = ["#BCBCBC", "#3A3A3A", 250, 237, ""]
let s:Grey35       = ["#BCBCBC", "#585858", 250, 240, ""]
let s:Grey100      = ["#000000", "#FFFFFF",  16, 231, ""]
let s:Red3         = ["#FFFFFF", "#D70000", 231, 160, ""]

let g:airline#themes#powerline_shell_Alt#palette = {}

" NORMAL
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let s:airline_a_normal = s:DeepSkyBlue3
let s:airline_b_normal = s:Grey19
let s:airline_c_normal = s:Grey35

let g:airline#themes#powerline_shell_Alt#palette.normal = airline#themes#generate_color_map(s:airline_a_normal, s:airline_b_normal, s:airline_c_normal)
let g:airline#themes#powerline_shell_Alt#palette.normal.airline_warning = s:Red3

let g:airline#themes#powerline_shell_Alt#palette.normal_modified =
\{
\   "airline_c": s:Grey100
\}
let g:airline#themes#powerline_shell_Alt#palette.normal_modified.airline_warning = s:Red3

" INSERT
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let s:airline_a_insert = s:Red3
let s:airline_b_insert = s:Grey19
let s:airline_c_insert = s:Grey35

let g:airline#themes#powerline_shell_Alt#palette.insert = airline#themes#generate_color_map(s:airline_a_insert, s:airline_b_insert, s:airline_c_insert)
let g:airline#themes#powerline_shell_Alt#palette.insert.airline_warning = g:airline#themes#powerline_shell_Alt#palette.normal.airline_warning

let g:airline#themes#powerline_shell_Alt#palette.insert_modified = g:airline#themes#powerline_shell_Alt#palette.normal_modified
let g:airline#themes#powerline_shell_Alt#palette.insert_modified.airline_warning = g:airline#themes#powerline_shell_Alt#palette.normal.airline_warning

" REPLACE
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:airline#themes#powerline_shell_Alt#palette.replace = copy(g:airline#themes#powerline_shell_Alt#palette.insert)
let g:airline#themes#powerline_shell_Alt#palette.replace.airline_warning = g:airline#themes#powerline_shell_Alt#palette.insert.airline_warning

let g:airline#themes#powerline_shell_Alt#palette.replace_modified = g:airline#themes#powerline_shell_Alt#palette.insert_modified
let g:airline#themes#powerline_shell_Alt#palette.replace_modified.airline_warning = g:airline#themes#powerline_shell_Alt#palette.insert.airline_warning

" VISUAL
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:airline#themes#powerline_shell_Alt#palette.visual = copy(g:airline#themes#powerline_shell_Alt#palette.normal)
let g:airline#themes#powerline_shell_Alt#palette.visual.airline_warning = g:airline#themes#powerline_shell_Alt#palette.normal.airline_warning

let g:airline#themes#powerline_shell_Alt#palette.visual_modified = g:airline#themes#powerline_shell_Alt#palette.normal_modified
let g:airline#themes#powerline_shell_Alt#palette.visual_modified.airline_warning = g:airline#themes#powerline_shell_Alt#palette.normal.airline_warning

" Inactive buffer
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let s:airline_a_inactive = s:Grey15
let s:airline_b_inactive = s:Grey19
let s:airline_c_inactive = s:Grey23

let g:airline#themes#powerline_shell_Alt#palette.inactive = airline#themes#generate_color_map(s:airline_a_inactive, s:airline_b_inactive, s:airline_c_inactive)

" tabline extension
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:airline#themes#powerline_shell_Alt#palette.tabline =
\{
\   "airline_tablabel":           s:Grey19,
\   "airline_tab":                s:Grey19,
\   "airline_tabsel":             s:DeepSkyBlue3,
\   "airline_tabtype":            s:DeepSkyBlue3,
\   "airline_tabfill":            s:Grey35,
\   "airline_tabmod":             s:DeepSkyBlue3,
\   "airline_tabhid":             s:Grey35,
\   "airline_tabmod_unsel":       s:Grey100,
\
\   "airline_tabsel_right":       s:DeepSkyBlue3,
\   "airline_tab_right":          s:Grey35,
\   "airline_tabmod_right":       s:DeepSkyBlue3,
\   "airline_tabhid_right":       s:Grey35,
\   "airline_tabmod_unsel_right": s:Grey100
\}
